<?php

require_once (__DIR__ . '/config/database.php');
require_once (__DIR__ . '/includes/header.php');
echo GOOGLE_LINK
?>

    <main role="main">

        <div class="py-5 bg-light">
            <div class="container">
                <div class="">
                    <?php if (isset($_SESSION['user_id'])) { ?>
                    <p>
                        <a href="/src/user/logout.php" class="btn btn-success m-1">Logout</a>
                    </p>
                    <?php } else {?>
                    <p>
                        <a href="/src/user/new.php" class="btn btn-success m-1">Créer un compte</a>
                    </p>
                    <p>
                        <a href="<?=GOOGLE_LINK?>" class="btn btn-success m-1">Créer un compte avec google</a>
                    </p>
                    <p>
                        <a href="/src/user/login.php" class="btn btn-primary m-1">Login</a>
                    </p>

                    <p>
                        <a href="/src/user/login.php" class="btn btn-primary m-1">Login avec google</a>
                    </p>
                    <?php } ?>
                </div>
            </div>
        </div>

    </main>


<?php

require_once (__DIR__ . '/includes/footer.php');

?>
