<?php
require '../../vendor/autoload.php';
require '../../config/global.php';
use GuzzleHttp\Client;

if ($_GET) {
    $client = new Client([
      'timeout'  => 2.0,
    ]);
    $response = $client->request('GET','https://accounts.google.com/.well-known/openid-configuration');
    $discovery_json = json_decode((string)$response->getBody());
    $token_endpoint = $discovery_json->token_endpoint;
    $userinfo_endpoint = $discovery_json->userinfo_endpoint;
    $response = $client->request('POST',$token_endpoint,[
      'form_params' =>[
          'code'=>$_GET['code'],
          'client_id' => GOOGLE_ID,
          'client_secret'=>GOOGLE_SECRET,
          'redirect_uri'=>'http://localhost:8888/src/user/connect.php',
          'grant_type'=>'authorization_code'
      ]
    ]);
    $access_token = json_decode((string)$response->getBody())->access_token;
    $response = $client->request("GET", $userinfo_endpoint,[
        'headers' => [
          'Authorization' => 'Bearer'.$access_token
        ]
    ]);
    $response = json_decode((string)$response->getBody());
    if($response->email_verified === true){
      session_start();
      $_SESSION['email'] = $response->email;
      $_SESSION['user_id'] = $response->email;

      echo  $response->email." connected";
      header("Location:../product/index.php");
      exit();
    }
}

?>
