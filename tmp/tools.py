from flask import Flask
import numpy as np
import os
from PIL import Image
from io import BytesIO
import base64
from DB.Patients import Patients

def VerifAttr(List, AttrList):
    for key, value in List.items():
        if (not(key in  AttrList)):
            return False
    return True

def convert_to_png(DcmArray):
    #ds = pydicom.dcmread(file)
    ds = DcmArray
    #shape = ds.pixel_array.shape
    # Convert to float to avoid overflow or underflow losses.
    #image_2d = ds.pixel_array.astype(float)
    image_2d = DcmArray
    # Rescaling grey scale between 0-255
    image_2d_scaled = (np.maximum(image_2d,0) / image_2d.max()) * 255.0

    # Convert to uint
    image_2d_scaled = np.uint8(image_2d_scaled)
    img = Image.fromarray(image_2d_scaled)
    buffer = BytesIO()
    img.save(buffer,format="png")
    myimage = buffer.getvalue()
    return  base64.b64encode(myimage)

def convert_array(arr):
    i = 0
    ret = [None] * len(arr)
    while(i < len(arr)):
        ret[i] = str(convert_to_png(arr[i]))
        i += 1;
    return(ret)


def mipifyarr(arr, i):
    ret = np.array(arr)
    #ret = np.reshape(ret,(ret.shape[0], 1), order='C')
    ret[0] = ret.max(0)
    return ret[0]

def MipArr_(arr):
    i = 0
    arr_ = np.array(arr)
    npret = np.array(arr)
    while(i < len(arr_)):
        npret[i] = mipifyarr(arr, i)
        i += 1;
    return(npret)

def MipArr(arr):
    return [arr[s:].max(0) for s in range(len(arr))]

def GetUploadObj(form):
    patientId =  form["patientId"]
    uploadId =  form["uploadId"]
    token =  form["token"]
    Storing_Folder = ""
    try:
        userId = form["userId"]
    except:
        userId = "Connect"
    return {"patientId":patientId,
            "uploadId":uploadId,
            "token":token,
            "StoringFolder":"",
            "userId":userId,
            "criptSecret":"42"}

def CreatePatient(UploadObj):
    Pat = Patients.objects(PatientAppID=UploadObj["patientId"])
    if (Pat.count() == 0):
        print("Creating ....")
        print("+++++++++++++++++++++++++++++++++++++++++++++++++")
        Pat = Patients(PatientAppID=UploadObj["patientId"])
        Pat.save()
